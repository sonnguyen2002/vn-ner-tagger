<?php


$CONFIGURE["TAG_INFO"] = array(
    "LOC" => array(
        "button_text" => "LOCATION", 
        "css" => "location"),
    "PER" => array(
        "button_text" => "PERSON",
        "css" => "person"),
    "ORG" => array(
        "button_text" => "ORGANIZATION",
        "css" => "organization"),
    "TIME" => array(
        "button_text" => "TIME ",
        "css" => "time")
);

$arr_tag_begin = array();
$arr_tag_end = array();
$arr_css_list = array();

foreach ($CONFIGURE["TAG_INFO"] as $key => $value)
{
    $arr_tag_begin[] = "B-".$key;
    $arr_tag_end[] = "I-".$key;
    $arr_css_list["B-".$key] = $value["css"];
}

$CONFIGURE["TAG_LIST"] = json_encode($arr_tag_begin);
$CONFIGURE["TAG_LIST_END"] = json_encode($arr_tag_end);
$CONFIGURE["CSS_LIST"] = json_encode($arr_css_list);

