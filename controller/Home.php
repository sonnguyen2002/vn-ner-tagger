<?php

class HomeController {

    public function index() {
        $text = "Chị Minh ôm đứa con gái mới hơn hai tháng rưỡi tuổi nấc lên từng tiếng thảm thiết khi kể lại cho chúng tôi nghe về cái chết của chồng .
Từ đôi mắt thâm quầng mệt mỏi những giọt nước mắt nối tiếp chảy tràn trên khuôn mặt người quả phụ trẻ từng giọt lệ mặn chát nhỏ xuống người đứa bé còn đỏ hỏn chị đang ôm trước ngực .
Không chỉ nước mắt mồ hôi và máu của các thuyền viên đổ trên sàn tàu họ đã phải trả giá cho cuộc thoát nghèo bằng chính tính mạng của mình .
Chỉ riêng xã Cương Gián Hà Tĩnh đã có 10 thuyền viên tử nạn giữa trùng dương bao la .
Ở Nghi Hải Nghệ An cũng có những thuyền viên bỏ xác nơi biển khơi xa xôi .
Những đứa con không biết mặt cha .
Chiều cuối thu trời vùng biển Nghi Xuân ảm đạm .
Tiếng người vợ trẻ khóc thương chồng theo gió biển đi sâu hơn vào phía trong xóm .
Cả làng cả xã như đượm màu u ám trước cảnh bi ai tang tóc của gia đình chị Hoàng Thị Minh 25 tuổi thôn Bắc Sơn xã Cương Gián Hà Tĩnh .
Người vợ trẻ vẫn nấc lên từng tiếng não nề thảm thiết .
";;
        if (isset($_REQUEST["sentence"]))
            $text = $_REQUEST["sentence"];
        $data = array(
            "sentence" => $text,
            "json_result"=> "{}"
        );

        $VIEW = "./view/Home.php";
        require("./template/template.phtml");
    }
   
//    public function save()
//    {
//        $json_result = "";
//        $file = "default.json";
//        if (isset($_REQUEST["content"]))
//            $json_result = $_REQUEST["content"];
//        if (isset($_REQUEST["file"]))
//            $file = $_REQUEST["file"];
//
//        $json_a = json_decode($json_result, true);
//
//        # save the JSON
//        $file = "files/examples/" . $file;
//        file_put_contents($file, json_encode($json_a,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
//
//
//        $data = array(
//            "sentence" => $json_a["text"],
//            "json_result" => $json_result,
//            "files" => $this->get_files("files/examples")
//        );
//
//
//        $VIEW = "./view/Home.php";
//        require("./template/template.phtml");
//    }

    public function parse() {
        $text = "";
        if (isset($_REQUEST["sentence"]))
            $text = $_REQUEST["sentence"];

        $sentences_jstr = TaggerAPIController::do_tokenize(str_replace("_", " ", $text));
        $sentences = json_decode($sentences_jstr);
        $text = join("\n", $sentences);

        if ($text == "") {
            $text = "Error ! Cannot tokenize, please try again";
            $json_result = "{}";
        }
        else
            $json_result = TaggerAPIController::do_parse($text);



        $data = array(
            "sentence" => $text,
            "json_result" => $json_result,
        );

        $VIEW = "./view/Home.php";
        require("./template/template.phtml");
    }



}
