Bà Hillary Clinton, ứng viên tổng thống Mỹ đảng Dân chủ. Ảnh: Reuters.
"Hillary Clinton tự nguyện tham gia thẩm vấn sáng nay (ngày 2/7) về quá trình sử dụng email khi bà còn là ngoại trưởng", AFP dẫn lời Nick Merrill, người phát ngôn chiến dịch tranh cử của bà Clinton, cho biết trong một thông báo.
Theo ông Merrill, bà Clinton "hài lòng vì đã có cơ hội hỗ trợ Bộ Tư pháp sớm kết thúc điều tra".
"Bà ấy sẽ không bình luận thêm về cuộc thẩm vấn để tôn trọng quá trình điều tra", ông cho biết thêm.
ABC News đưa tin Bộ Tư pháp Mỹ cũng mong muốn hoàn tất cuộc điều tra trước khi đại hội đảng Dân chủ và Cộng hòa diễn ra. Đại hội đảng Cộng hòa bắt đầu vào ngày 18/7 tại Cleveland còn đại hội đảng Dân chủ khởi động vào ngày 25/7 tại Philadelphia.
FBI trước đó thẩm vấn một số cố vấn hàng đầu cho bà Clinton, bao gồm Cheryl Mills và Huma Abedin, chánh văn phòng và phó chánh văn phòng cựu ngoại trưởng Mỹ, nhằm xem xét liệu ứng viên tổng thống đảng Dân chủ có xử lý sai các thông tin tuyệt mật hay không.
Chiến dịch tranh cử tổng thống của bà Clinton từng vấp phải trở ngại sau khi bà bị phát hiện sử dụng email cá nhân cho việc công khi còn làm ngoại trưởng. Giới chuyên gia chỉ trích bà Clinton đã vi phạm luật liên bang.
Như Tâm