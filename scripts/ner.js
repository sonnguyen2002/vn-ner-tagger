function display(json_a, css_list, tag_list, tag_list_end)
{
/*    
    var css_list = {
        "B-LOC": "location",
        "B-TIME": "time",
        "B-ORG": "organization",
        "B-PER": "person"
    };
    var tag_list = ["B-LOC", "B-TIME", "B-ORG", "B-PER"];
    var tag_list_end = ["I-LOC", "I-TIME", "I-ORG", "I-PER"];
*/
    var tokens = "";
    var out = "";
    if (json_a["sentences"] == null)
        return "";
    for (var i = 0; i < json_a["sentences"].length; i++)
    {
        var sentence = json_a["sentences"][i];
        var tokens = "";
        for (var j = 0; j < sentence.length; j++)
        {
            var id = "w-" + i + "-" + j;
            var tag = sentence[j][1];
            var word = sentence[j][0];
            if (tag_list.indexOf(tag) >= 0)
            {
                var css = css_list[tag];
                tokens = id;
                if (j + 1 < sentence.length
                        && tag_list_end.indexOf(sentence[j + 1][1]) >= 0)
                {
                    
                    out = out + " <span class='" + css + "'><span id='" + id + "' class='i'>" + word + "</span>";
                }
                else
                {
                    out = out + " <span class='" + css + "'><span id='" + id + "' class='i'>" + word + "</span></span> <sub><button class='nerRemove' data='" + tokens + "' style='cursor:pointer'><img class='small-icon' src='css/images/delete.png' /></button></sub>";

                }
            }
            else
            {
                if (tag_list_end.indexOf(tag) >= 0) 
                {
                    tokens = tokens + ":" + id;
                    if (j + 1 < sentence.length)
                    {
                        
                        if (tag_list_end.indexOf(sentence[j + 1][1]) < 0)
                        {                            
                            out = out + " <span id='" + id + "' class='i'>" + word + "</span></span> <sub><button class='nerRemove' data='" + tokens + "' style='cursor:pointer'><img class='small-icon' src='css/images/delete.png' /></button></sub>";
                        }
                        else 
                        {
                            out = out + " <span id='" + id + "' class='i'>" + word + "</span> ";
                        }
                    }                  
                    else 
                    {
                        out = out + " <span id='" + id + "' class='i'>" + word + "</span></span> <sub><button class='nerRemove' data='" + tokens + "' style='cursor:pointer'><img class='small-icon' src='css/images/delete.png' /></button></sub>";
                    }

                }
                else
                {
                    out = out + " <span id='" + id + "' class='o'>" + word + "</span> ";
                }
                
            }
        }
        out = out + "<br/>";
    }

    return "<div class='box'>" + out + "</div>";
}

function setStyle(b, e, style)
{
    if (b && e)            
        b.nextUntil(e).css(style);

    if (b)
        b.css(style);
    if (e)
        e.css(style);            
}