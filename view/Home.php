<?php global $CONFIGURE; ?>
<script src="scripts/ner.js"></script>
<script>    
    var css_list = <?php echo $CONFIGURE["CSS_LIST"]; ?>;
    var tag_list = <?php echo $CONFIGURE["TAG_LIST"]; ?>;
    var tag_list_end = <?php echo $CONFIGURE["TAG_LIST_END"]; ?>;
    
    $(document).ready(function() {
        var json_result2 = <?php echo $data["json_result"]; ?>;
        delete json_result2.sentences_html;
        $("#divResult").html(display(json_result2, css_list, tag_list, tag_list_end));

        // $("#txtContent").val(JSON.stringify(json_result2, undefined, 2))
        $("#preJsonOutput").html(JSON.stringify(json_result2, undefined, 2));
        var beginElement = null;
        var endElement = null;
        var bId = null;
        var bS = null;
        var eId = null;
        var eS = null;        
                       
        var select = function(b, e, flag)
        {
            var selected_style = {"color": "blue", "font-weight": "bold"};      
            var normal_style = {"color": "black", "font-weight": "normal"};
            if (flag)
            {
                setStyle (b, e, selected_style);
                console.log(b.text());
                var array = [];
                var arrayID = [];
                if (e)
                {
                    array = b.nextUntil(e).map(function(){
                        return $.trim($(this).text());
                    }).get();                    
                    array.push(e.text());
                    
                    arrayID = b.nextUntil(e).map(function(){
                        return $.trim($(this).attr("id"));
                    }).get(); 
                    arrayID.push(e.attr("id"));
                }                
                
                array.unshift(b.text()); //add item into the begining of array
                arrayID.unshift(b.attr("id")); //add item into the begining of array
                
                $("#selectedText").html(array.join(" "));
                $("#selectedIds").val(arrayID.join(":"));
            }
            else 
            {                
                setStyle (b, e, normal_style);
                beginElement = null;
                endElement = null;
                $("#selectedText").html("<span style='color:gray; font-style: italic'>Please choose a span of text below </span>");
            }
        }
        
        var nerSelectEvent = function()
        {            
            var e = $(this);
            if (!beginElement)
            {
                var temparr = e.attr("id").split("-");
                bId = parseInt(temparr[2]);
                bS = parseInt(temparr[1]);
                beginElement = e;
                select(beginElement, endElement, true);                
            }
            else 
            {                  
                var temparr2 = e.attr("id").split("-");
                var tId = parseInt(temparr2[2]);
                var tS = parseInt(temparr2[1]);
                if (tS == bS)
                {
                    if (!endElement)
                    {
                        if (tId < bId)
                        {
                            endElement = beginElement;
                            beginElement = e;
                            eId = bId;
                            eS = bS;
                            bId = tId;
                            bS = tS; 
                            select(beginElement, endElement, true);  
                        }
                        else if (tId > bId)
                        {
                            endElement = e;
                            eId = tId; 
                            eS = tS;
                            select(beginElement, endElement, true);  
                        }
                        else
                        {
                            // toggle the current
                            select (beginElement, endElement, false);                            
                            beginElement = null;
                        }                            
                    }
                    else
                    {                        
                        select(beginElement, endElement, false);
                        
                        endElement = null;                        
                        beginElement = e;
                        bId = tId;
                        bS = tS;                  
                        select(beginElement, endElement, true);  
                    }        
                    
                                    
                }
                else 
                {                    
                    select(beginElement, endElement, false);
                    
                    var temparr3 = e.attr("id").split("-");
                    bId = parseInt(temparr3[2]);
                    bS = parseInt(temparr3[1]);
                    beginElement = e;
                    endElement = null;
                    select(beginElement, endElement, true);  
                }
            }
            

//            e.attr('class', "i");
            
//            console.log(begin + ":" + end);
//            console.log(e.html());
//            console.log(e.prev().html());            
            
        }
        
        var nerRemoveEvent = function()
        {
            var e = $(this);      
            var tokens = e.attr("data").split(":");
            for (var i = 0; i < tokens.length; i ++)
            {
                var arr = tokens[i].split("-");
                var sIndex = arr[1];
                var tIndex = arr[2];
                json_result2["sentences"][sIndex][tIndex][1] = "O";
                
            }            
            $("#divResult").html(display(json_result2, css_list, tag_list, tag_list_end));

            // $("#txtContent").val(JSON.stringify(json_result2, undefined, 2))
            $("#preJsonOutput").html(JSON.stringify(json_result2, undefined, 2));
            $(".nerRemove").click(nerRemoveEvent);
            $(".o").click(nerSelectEvent);
        };
        $(".nerRemove").click(nerRemoveEvent);
        $(".o").click(nerSelectEvent);
        
        var nerAddEvent = function()
        {
            var label = $(this).attr("data");
//            alert(label);
            var tokens =  $("#selectedIds").val().split(":");
            for (var i = 0; i < tokens.length; i ++)
            {
                var arr = tokens[i].split("-");
                var sIndex = arr[1];
                var tIndex = arr[2];
                if (i == 0)
                    json_result2["sentences"][sIndex][tIndex][1] = "B-" + label;
                else 
                    json_result2["sentences"][sIndex][tIndex][1] = "I-" + label;                
            }     
            $("#selectedText").html("<span style='color:gray; font-style: italic'>Add successfull ! Please choose a span of text below </span>");
            
            $("#divResult").html(display(json_result2, css_list, tag_list, tag_list_end));
            // $("#txtContent").val(JSON.stringify(json_result2, undefined, 2))
            $("#preJsonOutput").html(JSON.stringify(json_result2, undefined, 2));
            $(".nerRemove").click(nerRemoveEvent);
            $(".o").click(nerSelectEvent);
        };
        $(".addNER").click(nerAddEvent);
        
        function sticky_relocate() {
            if ($('#sticky-anchor').length)
            {
                var window_top = $(window).scrollTop();
                var div_top = $('#sticky-anchor').offset().top;
                if (window_top > div_top) {
                    $('#sticky').addClass('stick');
                    $('#sticky-anchor').height($('#sticky').outerHeight());
                } else {
                    $('#sticky').removeClass('stick');
                    $('#sticky-anchor').height(0);
                }
            }
        }

        $(function() {
            $(window).scroll(sticky_relocate);
            sticky_relocate();
        });

        var dir = 1;
        var MIN_TOP = 200;
        var MAX_TOP = 350;
    });


    
</script>

<h1>Vietnamese Named Entity Tagger </h1>
<h3>Nhập một câu/đoạn văn Tiếng Việt </h3>
<form method="post">
      <input type="hidden" name="controller" value="Home"/>

</form>

<form method="post">
    <input type="hidden" name="controller" value="Home"/>
    <input type="hidden" name="action" value="parse"/>    
    <textarea id="txtDocument" name="sentence" style="width: 100%" rows="5"><?php echo $data["sentence"];?></textarea>
    <br/>
    <center>
    <input type="submit" value="Parse" />
    </center>
</form>

<h3>Kết quả nhận diện thực thể (Có thể chỉnh sửa và lưu để làm dữ liệu huấn luyện). </h3>
<?php 
if (strlen($data["json_result"]) > 40) 
{ 
?>
    
<div id="sticky-anchor"></div>
<div id="sticky">
    <div class="box" style="background-color: greenyellow; height: 60px">
        <div style="float:left; height: 60px">
            <b style='color:black'>Thêm một thực thể mới: </b>
            <span id="selectedText" style="border:1px; padding: 2px; color:blue; font-weight: bold">
                <span style="color:gray; font-style: italic">
                    Please choose a span of text bellow and click the button to add new NER
                </span>                    
            </span> 
            <br/>
            <input type="hidden" id="selectedIds" />
            <?php 
                foreach ($CONFIGURE["TAG_INFO"] as $key => $value)
                {                    
                    echo "<button class='addNER' data='{$key}'> <span class='{$value['css']}'> {$value['button_text']}</span> </button>";
                }
            ?>

        </div>

    </div>

</div>

<?php 
}
?>

<div id="divResult">

</div>

<h3>JSON Output</h3>
<pre style="white-space: pre-wrap; border:1px solid; padding: 10px" id="preJsonOutput">

</pre>
